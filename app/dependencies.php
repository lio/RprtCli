<?php

use function DI\create;
use function DI\get;
use function DI\factory;

use RprtCli\Commands\InvoiceCommand;
use RprtCli\Commands\ReportCommand;
use RprtCli\Commands\TrackCommand;
use RprtCli\Utils\Configuration\ConfigurationInterface;
use RprtCli\Utils\Configuration\ConfigurationService;
use RprtCli\Utils\CsvReport\ReportCsv;
use RprtCli\Utils\CsvReport\ReportCsvInterface;
use GuzzleHttp\Client;
use Mpdf\Mpdf;
use RprtCli\Utils\Mailer\MailerInterface;
use RprtCli\Utils\Mailer\MailerService;
use RprtCli\Utils\PdfExport\PdfExportInterface;
use RprtCli\Utils\PdfExport\PdfExportService;
use RprtCli\Utils\TimeTrackingServices\YoutrackInterface;
use RprtCli\Utils\TimeTrackingServices\YoutrackService;
use Psr\Container\ContainerInterface;

# use Symfony\Component\Translation\Translator;
#use Symfony\Component\Translation\Loader\PoFileLoader;

// @TODO I could still use this d-i to have a plugin system.
// New plugins should have a dependencies.php in their folders
// and have dependencies defined there. Construct method
// should inject the right service.

return [
  'config.file' => 'rprt.config.yml',
  'config.path' => '~/.config/rprt-cli/',
  'default_locale' => 'en',
  'guzzle' => get(Client::class),
  // 'mpdf' => get(Mpdf::class),
  'mpdf' => factory(function (ContainerInterface $c) {
       return new Mpdf(['tempDir' => sys_get_temp_dir()]);
  }),
  // 'mpdf' => function () {
  //     return new Mpdf(['tempDir' => sys_get_temp_dir()]);
  // },
  ConfigurationInterface::class => get(ConfigurationService::class),
  ConfigurationService::class => create()->constructor(
    get('config.path'),
    get('config.file')
  ),
  'config.service' => get(ConfigurationInterface::class),
  YoutrackInterface::class => get(YoutrackService::class),
  YoutrackService::class => create()->constructor(
      get('config.service'),
      get('guzzle')
  ),
  'youtrack.service' => get(YoutrackInterface::class),
  PdfExportInterface::class => get(PdfExportService::class),
  PdfExportService::class => create()->constructor(
      get('config.service'),
      get('mpdf')
  ),
  'pdf_export.service' => get(PdfExportInterface::class),
  // 'locale' => get('config.service')->method('get', 'en'),
  // Translator::class => create()->constructor('sl')->method('addLoader', 'po', new PoFileLoader),
  // 'translator' => get(Translator::class),
  ReportCsvInterface::class => get(ReportCsv::class),
  ReportCsv::class => create()->constructor(
    get('config.service')
  ),
  'csv.report' => get(ReportCsvInterface::class),
  MailerInterface::class => get(MailerService::class),
  MailerService::class => create()->constructor(
      get('config.service'),
      get('pdf_export.service')
  ),
  'mailer' => get(MailerInterface::class),
  InvoiceCommand::class => create()->constructor(
    get('csv.report'),
    get('config.service'),
    get('youtrack.service'),
    get('pdf_export.service'),
    get('mailer')
  ),
  TrackCommand::class => create()->constructor(
      get('config.service'),
      get('youtrack.service')
  ),
  ReportCommand::class => create()->constructor(
      get('config.service'),
      get('youtrack.service'),
      get('csv.report')
  )
];
