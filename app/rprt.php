#!/usr/bin/env php
<?php

use Symfony\Component\Console\Application;
use RprtCli\Commands\InvoiceCommand;
use RprtCli\Commands\ReportCommand;
use DI\ContainerBuilder;

require __DIR__ . '/vendor/autoload.php';

$builder = new ContainerBuilder();
$builder->addDefinitions(__DIR__ . '/dependencies.php');
$container = $builder->build();

$application = new Application('Command Line Tool to process Youtrack Reports', '0.1.0');

$invoiceCommand = $container->get(InvoiceCommand::class);
$application->add($invoiceCommand);
$reportCommand = $container->get(ReportCommand::class);
$application->add($reportCommand);

$application->run();
