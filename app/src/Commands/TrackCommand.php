<?php

declare(strict_types=1);

namespace RprtCli\Commands;

use RprtCli\Utils\Configuration\ConfigurationInterface;
use RprtCli\Utils\TimeTrackingServices\YoutrackInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Track time from comfortableness of your terminal.
 *
 * Later connect this command to the Emacs and have your time tracked directly
 * from orgmode.
 */
class TrackCommand extends Command {

    protected $config;

    protected $youtrack;

    public function __construct(
        ConfigurationInterface $config,
        YoutrackInterface $youtrack,
        ?string $name = null
    ) {
        $this->config = $config;
        $this->youtrack = $youtrack;
        parent::__construct($name);
    }

    protected function configure(): void {
        $this->setName('youtrack');
        $this->setDescription('Track time into your youtrack service');
        $this->addUsage('rprt-cli youtrack --issue=[issue-name] --minutes=[minutes] --date=[days-ago] --description=[text] --work-type=[work-type]');
        // Options or arguments? Technically they are arguments but default value could be provided by config.
        // Options are more suitable.
    }



}
