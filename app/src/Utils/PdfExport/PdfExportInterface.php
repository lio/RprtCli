<?php

declare(strict_types=1);

namespace RprtCli\Utils\PdfExport;

/**
 * Handles exporting parsed csv data to pdf files.
 */
interface PdfExportInterface {

    /**
     * Retrieves path to template file either from command option, configuration
     * or from uer input.
     */
    public function getTemplatePath(): ?string;

    public function setTemplatePath(string $path): void;

    /**
     * Creates html table from parsed csv data.
     *
     * @param array $data
     *   First line is header. The rest of them is table body.
     */
    public function parsedDataToHtmlTable(array $data): ?string;

    /**
     * Reads the template file and replaces token values.
     */
    public function replaceTokensInTemplate(string $template_path, array $tokens): ?string;

    /**
     * Creates and export file.
     *
     * @param string $html
     *   Template file with tokens replaced.
     *
     * @return bool
     *   True if export was successfull.
     */
    public function pdfExport(string $html) : bool;

    /**
     * Goes through the whole process of creating a pdf for the invoice.
     *
     * @param array $nice_data
     *   Parsed csv report export data.
     *
     * @return string
     *   Path of the pdf file.
     */
    public function fromDefaultDataToPdf(array $nice_data, array $tokens = []): string;

    /**
     * Sets output file override via command paramater.
     *
     * @param string $output
     *   Path of the output pdf file ('/tmp/test.pdf').
     */
    public function setOutput(string $output): void;

    // @TODO support multiple templates by adding template id in config.
    // @TODO implement twig.
    /**
     * Get tokens to replace in the template.
     *
     * @param string $template_path
     *   Path to the template file (long term plan is to support multiple templates).
     * @param bool $skip_missing
     *   Just skip missing tokens.
     * @param array $runtime_tokens
     *   Provide tokens at runtime of the application (not supported yet). @TODO
     * @param string $config
     *   Config path to tokens.
     *
     * @return array
     *   Token keys and values array.
     */
    public function gatherTokensForTemplate(string $template_path, bool $skip_missing, array $runtime_tokens = [], string $config = 'export.token'): array;

}
