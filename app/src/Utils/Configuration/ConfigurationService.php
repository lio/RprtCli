<?php

declare(strict_types=1);

// src/Utils\Configuration/ConfigurationService.php

namespace RprtCli\Utils\Configuration;

use Symfony\Component\Yaml\Yaml;

use function explode;
use function file_exists;
use function var_dump;

/**
 * Read and write configuration.
 *
 * Check:
 *  - $HOME/.rprt.config.yaml
 *  - $HOME/.rprt/rprt.config.yaml
 *  - $HOME/.config/rprt-cli/rprt.config.yaml
 */
class ConfigurationService implements ConfigurationInterface
{
    protected const PATHS = [
        '/.',
        '/.config/rprt-cli/',
        '/.rprt/',
    ];

    protected $data;

    protected $default = null;

    protected $configFilePath;

    protected $configFileName;

    /**
     * Construct method.
     */
    public function __construct(string $filepath, string $filename)
    {
        $file                 = $filepath . $filename;
        $this->configFileName = $filename;
        $this->configFilePath = $this->findConfig($file);
        if ($this->configFilePath) {
            $this->
                getConfig();
        }
    }

  /**
   * Checks for config file.
   *
   * @param     string $filename
   *   Name of the configuration file.
   * @return string|bool
   *   Full path to config file or FALSE if it doesn't exist.
   */
    public function findConfig($filename)
    {
        if (file_exists($filename)) {
            return $filename;
        }
        foreach (self::PATHS as $path) {
            $fullPath = $_SERVER['HOME'] . $path . $this->configFileName;
            if (file_exists($fullPath)) {
                return $fullPath;
            }
        }
      // @TODO This should be some kind of error!
        var_dump('Config File Not Found!');
        return false;
    }

  /**
   * Get and read the configuration from file.
   */
    public function getConfig() : bool
    {
        if ($this->configFilePath) {
            $config     = Yaml::parseFile($this->configFilePath);
            $this->data = $config;
            return true;
        }
        // Maybe write an exception for missing config.
        // Ask for reconfiguration.
        // @TODO This should be some kind of error!
        var_dump('Config File Path not found!');
        return false;
    }

    /**
     * Get a specific configuration for key.
     *
     * @param string     $key
     *   Config key.
     * @param null|mixed $default
     *   Default value if config for key is not yet specified.
     * @return mixed
     *   Data.
     */
    public function get($key, $default = null)
    {
        $this->default = $default;
        $segments      = explode('.', $key);
        $data          = $this->data;
        foreach ($segments as $segment) {
            if (isset($data[$segment])) {
                $data = $data[$segment];
            } else {
                $data = $this->default;
                break;
            }
        }
        return $data;
    }

    /**
     * Checks if key exists in the configuration file.
     *
     * @param string $key
     *   Key to check for.
     *
     *   Value of configuration key is not equal to default.
     */
    public function exists($key) : bool
    {
        return $this->get($key) !== $this->default;
    }
}
