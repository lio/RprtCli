<?php

declare(strict_types=1);

// src/Utils/Configuration/ConfigurationInterface.php

namespace RprtCli\Utils\Configuration;

interface ConfigurationInterface
{
    /**
     * Get and read the configuration from file.
     */
    public function getConfig() : bool;

    /**
     * Get a specific configuration for key.
     *
     * @param string     $key
     *   Config key.
     * @param null|mixed $default
     *   Default value if config for key is not yet specified.
     * @return mixed
     *   Data.
     */
    public function get($key, $default = null);

    /**
     * Checks if key exists in the configuration file.
     *
     * @param string $key
     *   Key to check for.
     */
    public function exists($key) : bool;
}
