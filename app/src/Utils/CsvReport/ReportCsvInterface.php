<?php

declare(strict_types=1);

namespace RprtCli\Utils\CsvReport;

/**
 * Handles creating report data from csv file downloaded from youtrack service.
 */
interface ReportCsvInterface
{

    /**
     * Normal separator.
     */
    const SEPARATOR_SOFT = FALSE;

    /**
     * Medium separator. Next line bold.
     */
    const SEPARATOR_MEDIUM = NULL;

    /**
     * Next line should be bold and centered.
     */
    const SEPARATOR_HARD = 0;

    /**
     * Returns array of hours per configured projects.
     *
     * @todo - get data from variable.
     *
     *
     *   Project key as key and number of hours as value.
     */
    public function getInvoiceData(string $filePath): array;

    /**
     * Returns array of rows created from array of InvoiceElements.
     *
     * If row is null, it is meant to be a table separator.
     */
    public function generateTable(array $data) :array;

    /**
     * Data for default drunomics pdf export.
     *
     * @param array $data
     *   Parsed data from csv report.
     */
    public function arangeDataForDefaultPdfExport(array $data): array;
}
