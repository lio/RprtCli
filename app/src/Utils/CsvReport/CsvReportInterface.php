<?php

declare(strict_types=1);

namespace RprtCli\Utils\CsvReport;

/**
 * Handles creating report data from csv file downloaded from youtrack service.
 */
interface CsvReportInterface
{
    /**
     * Returns array of hours per configured projects.
     *
     * @todo - get data from variable.
     *
     *
     *   Project key as key and number of hours as value.
     */
    public function getInvoiceData(string $filePath) : array;

    /**
     * Data for default drunomics pdf export.
     *
     * @param array $data
     *   Parsed data from csv report.
     */
    public function arangeDataForDefaultPdfExport(array $data): array;

}
