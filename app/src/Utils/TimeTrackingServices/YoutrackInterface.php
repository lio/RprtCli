<?php

declare(strict_types=1);

namespace RprtCli\Utils\TimeTrackingServices;

interface YoutrackInterface
{
    /**
     * Check if client can sign into youtrack with provided token.
     */
    public function testYoutrackapi() : ?string;

    /**
     * Get the id of the report from configuration.
     */
    public function getReportId() : ?string;

    /**
     * Downloads report and returns file path.
     *
     * @param string $report_id
     *   Youtrack internal report id.
     *
     * @return NULL|string
     *   If fetch was unsuccssefull return false, otherwise the file path.
     */
    public function downloadReport(string $report_id) : ?string;

}
