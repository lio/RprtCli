<?php

declare(strict_types=1);

namespace RprtCli\ValueObjects;

class Expenses implements ExpensesInterface {

    /**
     * Expenses in current currency.
     */
    private float $value;

    /**
     * Name of the expenses;
     */
    private string $name;

    public function __construct(string $name, float $value) {
        $this->name = $name;
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
