<?php

declare(strict_types=1);

namespace RprtCli\ValueObjects;

class WorkInvoiceElement implements WorkInvoiceElementInterface {

    private float $time;

    /**
     * Project name.
     */
    private string $name;

    public function __construct(string $name, float $time) {
        $this->name = $name;
        $this->time = $time;
    }

    public function getTime() :float {
        return $this->time;
    }

    public function getName() :string {
        return $this->name;
    }

}
