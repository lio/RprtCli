<?php

declare(strict_types=1);

namespace RprtCli\ValueObjects;

interface WorkInvoiceElementInterface extends InvoiceElementInterface {

    public function getTime() :float ;

    /**
     * Get project name.
     */
    public function getName() :string ;
}
