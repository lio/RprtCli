<?php

declare(strict_types=1);

namespace RprtCli\ValueObjects;

interface ExpensesInterface extends InvoiceElementInterface {

    public function getValue() :float;

}
