<?php

declare(strict_types=1);

namespace RprtCli\ValueObjects;

/**
 * Main interface for invoice elements.
 */
interface InvoiceElementInterface {

    /**
     * Project or expenses name.
     */
    public function getName() :string;

}
