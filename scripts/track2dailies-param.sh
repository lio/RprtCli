#!/bin/bash

# Dependencies:
#   getopt

# Track drunomix and kurier dailies.
#   KUR-181 (everyday 9.30)
#   DEV-122 (tue-fri 10.00) - TEAM-6
#   DEV-360 (tracking time) - TEAM-14

# TO DO:
#   - Implement track cli (WIP)


track_help () {
    echo "Tracks dailies and other weekly meetings."
    echo -e "\t-d|--drunomics-daily  :: [TEAM-6] Drunomics daily -   Tue-Fri(10.00) - 15m."
    echo -e "\t-w|--drunomics-weekly :: [TEAM-1] Drunomics weekly -  Mon(11.00) -     1h."
    echo -e "\t-k|--kurier-daily     :: [KUR-181] Kurier daily -     Mon-Fri(9.30) -  15m."
    echo -e "\t-o|--devops-weekly    :: [DEV-1967] DevOps weekly -   Mon(14.30) -     60m."
    echo -e "\t-u|--drupal-weekly    :: [CTB-24] Drupal weekly -     Wed(14.00) -     30m."
    echo -e "\t-l|--ldp-weekly       :: [LDP-683] LDP weekly -       Tue(11.00) -     30m."
    echo -e "\t-v|--wv-weekly        :: [WV-31] WV weekly -          Wed(10.30) -     30m."
    echo -e "\t-e|--kurier-weekly    :: [KUR-181] Kurier weekly -    Wed(15.00) -      1h."
    echo -e "\t-p|--lupus-d-weekly   :: [CTB-55] Lupus dcpld weekly- Thu(11.00) -     30m."
    echo -e "\t-t|--tracking-time    :: [TEAM-14] Personal time tracking - 15m."
    echo -e "\t-s|--date             :: Date in timestamp-miliseconds. Defaults to [\$(date +%s)000]"
    echo -e "\tmissing :: Work type id. (Not supported yet)."
}

track_help_color () {
  green='\033[0;32m'
  cyan='\033[0;36m'
  clr='\033[0m'
  DAY_OF_WEEK=$(date +%u -d "@$DATE")
  COLOR_TUE_FRI=$clr
  COLOR_MON=$clr
  COLOR_TUE=$clr
  COLOR_WED=$clr
  COLOR_THU=$clr
  COLOR_ALL_DAYS=$green
  COLOR_PERSONAL=$clr
  if [ "$DAY_OF_WEEK" == "1" ] ; then
    COLOR_MON=$green
    COLOR_MON_2=$cyan
  elif [ "$DAY_OF_WEEK" == "2" ] ; then
    COLOR_TUE=$green
    COLOR_TEAM6=$green
    COLOR_TUE_FRI=$green
  elif [ "$DAY_OF_WEEK" == "3" ] ; then
    COLOR_WED=$green
    COLOR_TUE_FRI=$green
  elif [ "$DAY_OF_WEEK" == "4" ] ; then
    COLOR_THU=$green
    COLOR_TUE_FRI=$green
  elif [ "$DAY_OF_WEEK" == "5" ] ; then
    COLOR_TUE_FRI=$green
  else
    COLOR_ALL_DAYS=$clr
  fi
  if [ "$DEV360" == "1" ] ; then
    COLOR_PERSONAL=$green
  fi
  echo "Tracks dailies and other weekly meetings."
  echo -e "\t-w|--drunomics-weekly :: ${COLOR_MON}[TEAM-1]${clr} Drunomics weekly - ${COLOR_MON}Mon(11.00)${clr} - ${DEV46}m."
  echo -e "\t-o|--devops-weekly    :: ${COLOR_MON}[DEV-1967]${clr} DevOps weekly - ${COLOR_MON}Mon(14.00)${clr} - ${DEVOPS}m."
  echo -e "\t-d|--drunomics-daily  :: ${COLOR_TUE_FRI}[TEAM-6]${clr} Drunomics daily - ${COLOR_TUE_FRI}Tue-Fri(10.00)${clr} - ${DEV122}m."
  echo -e "\t-k|--kurier-daily     :: ${COLOR_ALL_DAYS}[KUR-181]${clr} Kurier daily - ${COLOR_ALL_DAYS}Mon-Fri(9.30)${clr} - ${KUR181}m."
  echo -e "\t-l|--ldp-weekly       :: ${COLOR_TUE}[LDP-683]${clr} LDP weekly - ${COLOR_TUE}Tue(11.00)${clr} - ${LDP683}m."
  echo -e "\t-u|--drupal-weekly    :: ${COLOR_WED}[CTB-24]${clr} Drupal weekly - ${COLOR_WED}Wed(14.00)${clr} - ${CTB24}m."
  echo -e "\t-v|--wv-weekly        :: ${COLOR_WED}[WV-31]${clr} WV weekly - ${COLOR_WED}Wed(10.30)${clr} - ${WV31}m."
  echo -e "\t-e|--kurier-weekly    :: ${COLOR_WED}[KUR-182]${clr} Kurier weekly - ${COLOR_WED}Wed(15.00)${clr} - ${KUR182}."
  echo -e "\t-p|--lupus-d-weekly   :: ${COLOR_THU}[CTB-55]${clr} Lupus dcpld weekly- ${COLOR_WED}Thu(11.00)${clr} - ${CTB55}."
  echo -e "\t-t|--tracking-time    :: ${COLOR_PERSONAL}[TEAM-14]${clr} Personal time tracking - 15m."
  echo -e "\t-s|--date             :: Date in timestamp-miliseconds. Defaults to [\$(date +%s)000]"
  echo -e "\tmissing               :: Work type id. (Not supported yet)."
}

DEV122=15 # now TEAM-6
DEV46=60  # now TEAM-1
KUR181=15
KUR182=60
WV31=30
LDP683=30
DEV360=0 # now TEAM-14
DEVOPS=60
CTB24=30
CTB55=30
DATE=$(date +%s)

# options may be followed by one colon to indicate they have a required argument
if ! options=$(getopt -u -o thd:w:k:e:l:v:s:o:u:p: -l help,time-tracking,drunomics-daily:,drunomics-weekly:,kurier-daily:,kurier-weekly:,ldp-weekly:,wv-weekly:,devops-weekly,drupal-weekly,lupus-d-weekly,date -- "$@")
then
    # something went wrong, getopt will put out an error message for us
    exit 1
fi

set -- $options

while [ $# -gt 0 ]
do
    case $1 in
    -t|--tracking-time) DEV360=1 ;;
    -d|--drunomics-daily) DEV122=$2 ; shift ;;
    -w|--drunomics-weekly) DEV46=$2 ; shift ;;
    -k|--kurier-daily) KUR181=$2 ; shift ;;
    -e|--kurier-weekly) KUR182=$2 ; shift ;;
    -l|--ldp-weekly) LDP683=$2 ; shift ;;
    -v|--wv-weekly) WV31=$2 ; shift ;;
    -o|--devops-weekly) DEVOPS=$2 ; shift ;;
    -u|--drupal-weekly) CTB24=$2 ; shift ;;
    -p|--lupus-d-weekly) CTB55=$2 ; shift ;;
    -h|--help) TRACK_HELP=1;;
    -s|--date) DATE=$2 ; shift ;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*) break;;
    esac
    shift
done


if [ "$TRACK_HELP" == "1" ] ; then
    track_help_color
else
  DAY_OF_WEEK=$(date +%u -d "@$DATE")
  if [ "$DAY_OF_WEEK" == "1" ] ; then
    echo "Monday: Kurier daily, Drunomics weekly, DevOps weekly:"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh KUR-181 $KUR181 'daily' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh TEAM-1 $DEV46 'weekly' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh DEV-1967 $DEVOPS 'weekly' "$(echo $DATE)000"
  elif [ "$DAY_OF_WEEK" == "2" ] ; then
    echo "Tuesday: Kurier daily, Drunomics daily, LDP weekly:"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh KUR-181 $KUR181 'daily + retro' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh TEAM-6 $DEV122 'daily' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh LDP-683 $LDP683 'ldp weekly' "$(echo $DATE)000"
  elif [ "$DAY_OF_WEEK" == "3" ] ; then
    echo "Wednesday: Kurier daily, Drunomics daily, Wirtschaftsverlag weekly, Kurier weekly:"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh KUR-181 $KUR181 'daily' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh TEAM-6 $DEV122 'daily' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh CTB-24 $CTB24 'drupal weekly' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh KUR-182 $KUR182 'Kurier weekly grooming' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh WV-31 $WV31 'weekly' "$(echo $DATE)000"
  elif [ $DAY_OF_WEEK == "4" ] ; then
    echo "Thursday: Kurier, Drunomics daily, Lupus Decoupled weekly:"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh CTB-55 $CTB55 'weekly' "$(echo $DATE)000"
  elif [ $DAY_OF_WEEK -gt 5 ] ; then
    echo "No dailies over the weekend."
  else
    echo "Youtrack drunomix & kurier dailies"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh KUR-181 $KUR181 'daily' "$(echo $DATE)000"
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh TEAM-6 $DEV122 'daily' "$(echo $DATE)000"
  fi
  if [ "$DEV360" == "1" ] ; then
    /home/lio/Projects/drunomix/ytest/scripts/ytrack.sh TEAM-14 15 'tracking time' "$(echo $DATE)000"
  fi
fi
