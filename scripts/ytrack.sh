#!/bin/bash

# Usage:
#  First export your youtrack api token (you can put this in .bashrc)
#   
#    export YTOKEN=<your-youtrack-token-created-in-yt-hub>
#
#  All parameters are optional but the sort order defines priority
#
#    ytrack.sh <issue-name> <time> <description> <timestamp-miliseconds>
#
#  or: 
#
#    ytrack.sh [DEV-122] [15] [daily] [1631567581342] 
#
# @TODO: 
#   - Figure a nice way to set type: Development(57-0), Project management, Testing & Review(57-1)...
#   - Write a proper script with parameters and everything

# Development (57-0)
# Testing, Review (57-1)

track_help () {
    echo "Youtrack command line tool."
    echo -e "\t\$1 :: Ticket name. Defaults to [DEV-122]."
    echo -e "\t\$2 :: Number of minutes. Defaults to [15]."
    echo -e "\t\$3 :: Work item description. Defaults to [daily]."
    echo -e "\t\$4 :: Date in timestamp-miliseconds. Defaults to [\$(date +%s)000]" 
    echo -e "\t\$5 :: Work type id. (Not supported yet)."
}

# FLAGS SETUP
while getopts "h" option
do
    case "${option}"
    in
        h) TRACK_HELP=1;;
    esac
done
shift $((OPTIND -1))

if [ "$TRACK_HELP" == "1" ] ; then
    track_help
else
    YT_ISSUE=${1:-DEV-122}
    YT_MINUTES=${2:-15}
    YT_TEXT=${3:-daily}
    CURRENT_DATE="$(date +%s)000"
    YT_DATE=${4:-$CURRENT_DATE}
    YT_WORK_TYPE=${5:-null}

    YT_USER_RESP=$(curl -s 'https://drunomics.myjetbrains.com/youtrack/api/admin/users/me?fields=id,mail,fullName' -H "Authorization: Bearer $YTOKEN" -H 'Cache-Control: no-cache')

    YT_USER_ID=$(echo $YT_USER_RESP | jq .id)
    YT_USER_ID="${YT_USER_ID:1:-1}"
    YT_USER_NAME=$(echo $YT_USER_RESP | jq .fullName)

    YT_ISSUE_ID=$(curl -s "https://drunomics.myjetbrains.com/youtrack/api/issues?fields=id,idReadable&query=$YT_ISSUE" -H "Authorization: Bearer $YTOKEN" -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -H 'Accept: application/json' | jq -c ".[] | select ( .idReadable == \"$YT_ISSUE\") | .id")
    YT_ISSUE_ID="${YT_ISSUE_ID:1:-1}"

    echo "issue: $YT_ISSUE"
    echo "issue_id: $YT_ISSUE_ID"
    echo "minutes: $YT_MINUTES"
    echo "text: $YT_TEXT"
    echo "date: $YT_DATE"
    echo "user: $YT_USER_NAME"
    echo "type: $YT_WORK_TYPE"

    # curl -s -X POST "https://drunomics.myjetbrains.com/youtrack/api/issues/$YT_ISSUE_ID/timeTracking/workItems?fields=id,created,type(name,id),author(name,id),duration(minutes,presentation)" -H "Authorization: Bearer $YTOKEN" -H 'Cache-Control: no-cache' -d "{\"author\": {\"id\":\"$YT_USER_ID\"},\"duration\":{\"minutes\":$YT_MINUTES},\"text\":\"$YT_TEXT\",\"date\":$YT_DATE,\"type\":{\"id\":\"$YT_WORK_TYPE\"}}" -H 'Content-Type: application/json' | jq .
    curl -s -X POST "https://drunomics.myjetbrains.com/youtrack/api/issues/$YT_ISSUE_ID/timeTracking/workItems?fields=id,created,type(name,id),author(name,id),duration(minutes,presentation)" -H "Authorization: Bearer $YTOKEN" -H 'Cache-Control: no-cache' -d "{\"author\": {\"id\":\"$YT_USER_ID\"},\"duration\":{\"minutes\":$YT_MINUTES},\"text\":\"$YT_TEXT\",\"date\":$YT_DATE}" -H 'Content-Type: application/json' | jq .
fi


