* Report cli

  Automate generating invoices from youtrack reports and other time-tracking
  related functionality.
  
** Usage

   ~./rprt.php invoice -y -p -s~

   This command would send an invoice for last month created from the template
   file and last month report from youtrack as an email attachment.

   ~./rprt.php report -r~

   Asks which report to print from the list of your reports and then prints out
   a table with that report.
     
** Install/Getting started

   Get ~.phar~ file from r, run configuration wizzard (planned for version 1.0).
   

   Before version 1.0 is released you have to navigate into ~/app~ directory
   and call command through ~.rprt.php~ file.
   
*** Requirements

    1. You have to create a youtrack API token.
    2. You have to configure rprt-cli app.
       - Default folder that app checks for config file is in ~~/.config/rprt-cli/~.
    3. You have to allow [[https://support.google.com/accounts/answer/6010255?hl=en][less secure applications]] in your gmail if you are using
       that email provider.

*** Building a phar

    php box: https://github.com/box-project/box

    ~./box.phar compile~

** Specs
   A simple Console Command that prints out monthly report from youtrack.

   Simple configuration:
   - authentication token value
   - project categories
   - hourly wage
   - folder for reports
     

** Components
   - report service - provides a csv of a report
     - local csv file read (provided via argument)
     - youtrack api request (with [[https://docs.guzzlephp.org/en/stable/][Guzzle]] and [[https://github.com/cybercog/youtrack-rest-php][Youtrack API php client]] )
       - Authentication: https://www.jetbrains.com/help/youtrack/devportal/Manage-Permanent-Token.html#obtain-permanent-token
       - API documentation: https://www.jetbrains.com/help/youtrack/devportal/youtrack-rest-api.html
   - parse report
     - compute hours per configured project categories from report
     - output a table
   - Create an invoice
     - send the invoice (to Wolfgang)

*** ValueObjects

    
*** List my reports

#+begin_example bash
curl 'https://drunomics.myjetbrains.com/youtrack/api/reports?$top=-1&fields=id,name,own,owner(login,name)&query=owner:+%7Bme%7D' -H "Authorization: Bearer $YTOKEN" -H 'Cache-Control: no-cache' | jq '.[] | select( .own == true )'
#+end_example

** Motivation
   - practice php
   - automatization of repetitive task
     - is it worth it? according to xkcd table - you should spend 1 day for
       automating a monthly task that takes 30 minutes. That would be if only
       I'd be using this app. If it get's picked up, by 5 others - it is worth
       5 days of development.
   - remove errors from reports
     
** Plan

*** most current
    1. For version 0.6.7
       - [X] data value objects
       - phar file
       - [X] additional expenses
    2. For version 1.0
       - plugin system
         - for time tracking services
         - for reports
       - symfony/config & symfony/di components

*** current   

    1. For Version 1.0
       - Track Command (track time from your cli)
       - Report Command (read reports)
       - Improve code and code style
       - ~.phar~ file for simple app shipping  
    2. Version 2.0
       - Implement templateing service
       - Further code improvements
       - Configuration Wizzard
    3. Version 3.0
       - Emacs package to track directly from orgmode
       - Add tests


*** old   
   1. Basic structure of the cli-app
      1. App preparation
         - nice specifications
         - composer project
         - autoloading & dependency injection
      2. Report service
         - read a local csv file
      3. Parse report
         - read configuration (project categories, hourly wage)
         - simple table output
   2. First round of enhancements
      1. Tests
      2. youtrack request
      3. create composer package, create executable
   3. Second round of enhancements
      1. Invoice output
      2. configuration wizard
        


** Learning
   - https://www.youtube.com/watch?v=aCqM9YnjTe0
     - Choices (~new ChoiceQuestion~)
     - ~addOption('config')~

** API calls

   
*** Get csv file

    ~curl 'https://drunomics.myjetbrains.com/youtrack/api/reports/83-554/export/csv?&$top=-1' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H "Authorization: Bearer $TKN" > ~/Documents/Drunomics/workhours/2021/21-09.csv~

